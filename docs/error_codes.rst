Error Codes
===========

This page provides detailed explanations of various error codes.


.. _error_1000:

1000
----

**Description**: The parameter file or script could not be loaded.

**Resolution**: Check and correct the name of your script or parameter file and try again.

.. _error_1001:

1001
----

**Description**: Gas particles exist in the input, but hydro is disabled

**Resolution**: Enable an appropriate hydro module or use an input without gas particles.

.. _error_1002:

1002
----

**Description**: INTERNAL error: message size mismatch.

**Resolution**: Contact a developer and provide the full error message include source file and line number.
This information is important as this error code can have multiple causes.

.. _error_1003:

1003
----

**Description**: Filename buffer exceeded.

**Resolution**: Check the path and filenames lengths, as one has been exceeded.


.. _error_1004:

1004
----

**Description**: EOSlib or one of the libraries used by EOSlib are outdated.

**Resolution**: Pull new versions of EOSlib and/or the libraries used by EOSlib and recompile pkdgrav3. The specific library that is outdated is specified in the line above the error.


.. _error_1005:

1005
----

**Description**: Explanation for error 1005.

**Resolution**: Details for fixing the issue.